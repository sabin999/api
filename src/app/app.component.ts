import { Component, OnInit } from '@angular/core';
import { MyserviceService } from "./service/myservice.service";



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{

  title = 'API';
  user:any;
  constructor(private myserviceService:MyserviceService){} 

  ngOnInit(){
    this.myserviceService.getUser().subscribe(
      (user:any)=>{console.log(user);
      this.user = user.results[0];
      },
    );
  }
}
